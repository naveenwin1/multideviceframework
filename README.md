# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* As there are many devices keep adding to the market at a high rate with different feature and different screen resolution, the users are browsing and using application in many devices, to maintain a good user experience with their application companies are spending huge cost in developing different web interface for different devices. This has given increase to the complexity of the code, which is leading to high maintenance cost. 
The Web UI framework will be useful as the developer needs to define what components needed and how, the framework will take care of generating the code for different devices which will reduce the developers work. Since there are many frameworks which are addressing this issue. The framework developed will be focusing on solving the performance of loading web page by pre generation of HTML and CSS. 

* 0.01
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Diagrams Link###

BlockDiagram
http://www.gliffy.com/go/publish/image/10844153/L.png

Sax Parser Sequence Diagram
http://www.gliffy.com/go/publish/image/10844719/L.png

Builder Sequence Diagram
http://www.gliffy.com/go/publish/image/10844835/L.png

Transcode Sequence Diagram
http://www.gliffy.com/go/publish/image/10844895/L.png

Main Template sequence Diagram
http://www.gliffy.com/go/publish/image/10844961/L.png

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact